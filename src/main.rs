use anyhow::{Context, Result};
use clap::{App, Arg};
use handlebars::Handlebars;

use serde_json::Map;
use serde_json::Value;


//use std::process;

fn main() -> Result<()> {
    // parse command line argument with clap 2.33
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::with_name("FORMAT")
                .short("f")
                .long("format")
                .help("value file format: toml (default), yaml, json")
                .takes_value(true)
        )
        .arg(
            Arg::with_name("VALUES_FILE")
                .help("The file with values to apply to the template")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("TEMPLATE_FILE")
                .help("The template file")
                .required(true)
                .index(2),
        )
        .get_matches();

    let template_file = matches.value_of("TEMPLATE_FILE").unwrap();
    let values_file = matches.value_of("VALUES_FILE").unwrap();
    let format = matches.value_of("FORMAT").unwrap_or("toml");

    //load values files into a String
    let raw_values = std::fs::read_to_string(values_file).with_context(|| format!("Failed to read value file {}",values_file))?;

    // parse values
    let values: Map<String, Value> = match format {
        "json" => serde_json::from_str(&raw_values)
            .with_context(|| format!("Failed understand your values. Parsing json format from file: {}", values_file))?,
        "yaml" => serde_yaml::from_str(&raw_values)
            .with_context(|| format!("Failed understand your values. Parsing yaml format  from file: {}", values_file))?,
        "toml" => toml::from_str(&raw_values)
            .with_context(|| format!("Failed understand your values. Parsing toml format from file: {}", values_file))?,
        _ => anyhow::bail!("You specified an unsupported format on the command line: {}",format)
    };

    // load & register template file
    let mut handlebars = Handlebars::new();
    handlebars
        .register_template_file("template", template_file)
        .with_context(|| format!("Failed to register template file: {}", template_file))?;

    //do the work
    println!("{}", handlebars.render("template", &values)
        .with_context(|| format!("Failed to render template file: {}", template_file))?
    );

    Ok(())
}
